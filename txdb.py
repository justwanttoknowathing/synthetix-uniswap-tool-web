from sqlite3 import connect as sqlconnect
import logging

class DB:
    def __init__(self, safe=True):
        self.conn, self.c = self.setup_database(safe)

    def setup_database(self, safe):
        conn = sqlconnect('/home/ubuntu/synthetix-uniswap-tool/txdb.sqlite', check_same_thread=safe)
        c = conn.cursor()
        c.execute('create table if not exists txs ("tx" string, "block" int, "sender" string, "value" float, "type" string, "starteth" float, "endeth" float, "startseth" float, "endseth" float, primary key("tx"));')
        c.execute('create index if not exists txs_block on txs ("block");')
        return conn, c

    def add(self, tx, block, sender, value, txtype, starteth, endeth, startseth, endseth):
        logging.info('tx: %s, block: %i, sender: %s, value: %s, type: %s, ETH: %s -> %s, sETH: %s -> %s' % (tx, block, sender, value, txtype, starteth, endeth, startseth, endseth))
        self.c.execute('insert into txs values (?, ?, ?, ?, ?, ?, ?, ?, ?);', (tx, block, sender, value, txtype, starteth, endeth, startseth, endseth))
        self.conn.commit()

    def is_repeat(self, tx):
        self.c.execute('select tx from txs where tx=?', (tx,))
        return len(self.c.fetchall()) >= 1

    def query(self, q, p=None):
        if p is None:
            self.c.execute(q)
        else:
            self.c.execute(q, p)
        return self.c.fetchall()
