#!/usr/bin/python3

from flask import Flask, render_template, abort, request
from txdb import DB
from copy import deepcopy

app = Flask(__name__)

@app.route('/logs')
def logs():
    with open('/home/ubuntu/synthetix-uniswap-tool/analyze.py.log') as f:
        lines = [{'message': line.strip()} for line in f.readlines()]
        return render_template('logs.html', lines=lines[-50:])

@app.route('/arb-rewards/<startblock>')
def arb_rewards(startblock):
    try:
        startblock = int(startblock)
    except:
        abort(403)

    data = db.query('select sender, starteth / startseth ratio, tx, block from txs where block >= ? and type = "ETH -> sETH swap" and ratio <= 0.99 order by block asc;', (startblock,))
    goodarbers = [{'block': row[3], 'address': row[0], 'ratio': row[1], 'tx': row[2]} for row in data]
    return render_template('arb-rewards.html', rewards=goodarbers, startblock=startblock)

@app.route('/staking-rewards/<startblock>/<endblock>')
def staking_rewards(startblock, endblock):
    try:
        startblock = int(startblock)
        endblock = int(endblock)
    except:
        abort(403)

    data = db.query('select type, block, sender, starteth, endeth, tx from txs where type in ("Contribution", "Withdrawl") order by block asc;')

    uni = 0.0
    balances = {}
    start_balances = None
    disqualified = []
    cheaters = []
    start, end = (False, False)
    for row in data:
        txtype, block, sender, starteth, endeth, tx = row
        value = endeth - starteth

        if block >= endblock and not end:
            end = True

        if sender not in balances:
            balances[sender] = 0.0

        if uni <= 0.00000000001:
            delta = value
        else:
            delta = value * uni / starteth
        uni += delta
        balances[sender] += delta

        if balances[sender] < -0.00000000001:
            cheaters.append({'address': sender, 'tx': tx})

        if txtype == 'Withdrawl' and start and not end:
            disqualified.append({'address': sender, 'tx': tx})

        if block >= startblock and not start:
            startbalances = deepcopy(balances)
            start = True

    if not start:
        startbalances = balances

    qbalances = deepcopy(startbalances)
    unisum = sum(startbalances.values())

    nodust = request.args.get('nodust') == 'True'
    if nodust:
        for i in startbalances:
            if startbalances[i] < 1:
                disqualified.append({'address': i, 'tx': 'DUST'})

    dqlist = [i['address'] for i in disqualified]
    qbalances = {k: v if k not in dqlist else 0 for k,v in qbalances.items()}

    total = sum(qbalances.values())
    total = 0.00000001 if total == 0 else total

    rewards = [{'address': i, \
                'balance': '' if i in dqlist else round(qbalances[i], 3), \
                'percent': round(qbalances[i]/total*100, 3), \
                'SNX': round(qbalances[i]/total*72000, 2), \
                'disqualifiedbalance': '' if i not in dqlist else round(startbalances[i], 3)} \
        for i in sorted(startbalances) if startbalances[i] > 0.00000001]

    weeks = []
    i = 0
    with open('keyblocks') as f:
        for line in f:
            start, end, note = line.split('\t')
            i += 1
            weeks.append({'week': i, 'start': start, 'end': end, 'note': note})

    return render_template('staking-rewards.html', rewards=rewards, disqualified=disqualified, startblock=startblock, endblock=endblock, nodust=nodust, weeks=weeks, unisum=unisum, cheaters=cheaters)

if __name__ == '__main__':
    db = DB(safe=False)
    app.run(debug=False, threaded=True, host='0.0.0.0')
