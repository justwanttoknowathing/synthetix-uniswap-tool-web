#!/usr/bin/python3

from bs4 import BeautifulSoup as bs
from requests import get as get
from re import findall, sub
from txdb import DB
import argparse
import logging
import sys

# EC2 can't python-requests to etherscan, so this is a hack url using a port forward and /etc/hosts
etherscan = 'https://etherscan.io:44333'

class Transaction:
    def __init__(self, tx):
        self.tx = tx
        self.raw = self.download()
        self.txtype, self.starteth, self.endeth, self.startseth, self.endseth = self.parse()

    def download(self):
        logging.info('Downloading state differences for %s' % self.tx)
        return get('%s/accountstatediff?m=normal&a=%s' % (etherscan, self.tx)).text

    def parse(self):
        soup = bs(self.raw, features='lxml')
        a = soup.find(id='0x4740c758859d4651061cc9cdefdba92bdc3a845d')
        if a is None:
            return ('internal failure', 0, 0, 0, 0)
        try:
            starteth, endeth = [float(findall(r'[0-9\.,]+', i.decode())[0].replace(',','')) for i in a.find_next_siblings('td')[2:4]]
        except AttributeError:
            return ('Failure', None, None, None, None)

        seth_blocks = soup(text='0x8361f6b072fc72af560d737ec174e72298ab36b34ff64672b91a6a862a52c723')[0].find_parent().find_parent().find_parent('div')
        startseth, endseth = [None, None]
        for p in seth_blocks:
            if 'before' in p.decode():
                startseth = int([findall(r'0000[0-9a-f]+', i)[0] for i in p.find_all('span')[-1]][0], 16)/10**18
            elif 'after' in p.decode():
                endseth = int([findall(r'000[0-9a-f]+', i)[0] for i in p.find_all('span')[-1]][0], 16)/10**18

        txtype = None
        if endeth > starteth and endseth > startseth:
            txtype = "Contribution"
        elif endeth < starteth and endseth > startseth:
            txtype = 'sETH -> ETH swap'
        elif endeth > starteth and endseth < startseth:
            txtype = 'ETH -> sETH swap'
        elif endeth == starteth and endseth == startseth:
            txtype = 'Failure'
        elif endeth < starteth and endseth < startseth:
            txtype = 'Withdrawl'
        else:
            raise RuntimeError('cannot categorize transaction')

        return (txtype, starteth, endeth, startseth, endseth)

class Index:
    def __init__(self, db):
        self.db = db
        self.page = 0
        self.done = False
        while not self.done:
            self.page += 1
            self.download()
            self.parse()

    def download(self):
        logging.info('Downloading index page %i' % self.page)
        try:
            self.raw = get('%s/txs?a=0x4740c758859d4651061cc9cdefdba92bdc3a845d&p=%i' % (etherscan, self.page)).text
        except:
            logging.error('Failed to get index!!')
            sys.exit(1)

    def parse(self):
        soup = bs(self.raw, features='lxml')
        rows = soup.find('div').find('table').find('tbody').find_all('tr')
        self.done = (len(rows) < 50)
        for i in rows:
            tx, block, _, _, sender, _, _, value, _ = i.find_all('td')
            tx = str(tx).split('/tx/')[1][:66]
            block = int(str(block).split('/block/')[1][:9].rstrip('">'))
            sender = str(sender).split('/address/')[1][:42]
            value = float(sub('[^0-9\.]', '', str(value)))
            if self.db.is_repeat(tx):
                self.done = True
                logging.info('Saw repeat tx %s, all done' % tx)
                return
            while True:
                details = Transaction(tx)
                if details.txtype != 'internal failure':
                    break
                logging.warn('Got junk from etherscan, retrying in a few seconds')
                sleep(3)
            self.db.add(tx, block, sender, value, details.txtype, details.starteth, details.endeth, details.startseth, details.endseth)

def main():
    parser = argparse.ArgumentParser(description='Uniswap sETH/ETH pool: daemon for backend updates')
    parser.add_argument("-v", "--verbose", help="be verbose", action="store_true")
    args = parser.parse_args()
    if args.verbose:
        logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s %(funcName)12s\t%(message)s')
    db = DB()
    index = Index(db)

if __name__ == '__main__':
    main()
